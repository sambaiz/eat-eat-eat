class PlacesController < ApplicationController
  respond_to :json

  def index
    respond_with Place.all
  end

  def show
    respond_with Place.find(params[:id])
  end

  def create
    respond_with Place.create(params[:place])
  end

  def update
    respond_with Place.update(params[:id], params[:place])
  end

  def destroy
    respond_with Place.destroy(params[:id])
  end
end
