class Event < ActiveRecord::Base
  attr_accessible :created_at, :owner, :place, :schedule_for, :title

  belongs_to :owner, :class_name => "User", :foreign_key => 'user_id'
  belongs_to :place
  has_many :entries
  has_many :entrants, :source => 'user', :through => :entries
end
