class User < ActiveRecord::Base
  attr_accessible :icon_url, :name, :profile_url, :token, :email

  # assosications
  has_many :events
  has_many :entry_association, :class_name => 'Entry'
  has_many :entries, :source => 'event', :through => :entry_association

  # validations
#  validates_presence_of :name, :icon_url
end
