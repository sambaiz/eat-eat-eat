class EatEatEat.Routers.Events extends Backbone.Router

  routes:
    '' : 'index'
    'new': 'new'
    ':id' : 'show'

  index: ->
    view = new EatEatEat.Views.EventsIndex(el: "#container")
    view.render()

  new: ->
    view = new EatEatEat.Views.EventsNew(el: "#container")
    view.render()

  show: (id)->
    alert 'show/' + id
