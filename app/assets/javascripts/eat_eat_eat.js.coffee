window.EatEatEat =
  Models: {}
  Collections: {}
  Views: {}
  Routers: {}
  initialize: ->
    new EatEatEat.Routers.Events()
    Backbone.history.start()

$(document).ready ->
  EatEatEat.initialize()
