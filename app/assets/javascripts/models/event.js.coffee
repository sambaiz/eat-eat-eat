class EatEatEat.Models.Event extends Backbone.Model
  url: "/api/events"
  defaults:
    owner: null
    title: ''
    created_at: new Date
    schedule_for: null
    entrants: []
    comments: []

  validate: (attrs) ->
    console.log 'do validate!'
    if attrs.hasOwnProperty('created_at') and !_.isDate(attrs.created_at)
      'Event.created_at must be a Date object.'
