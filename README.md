# About

Webサービス「ご飯食べよう！」はご飯を一緒に食べる人を探したり、作ってくれる人を見つけたりするサービスです。

# 前提知識

* Git
* Ruby on Rails
* HTML5
* CSS
* Backbone.js
* jQuery
* CoffeeScript
* Less
* Jasmine 
* Bootstrap

# 準備

1. fork 
2. `git clone _YOUR-REPOSITORY-URL_`
3. `bundle install --without production staging`
4. `rails server`
5. access http://localhost:3000/
6. Install PhantomJS
    * Mac: `brew install phantomjs`
    * Linux: `apt-get install phantomjs`
7. `bundle exec guard`

# 始めよう！
* public/index.html が本体
* app/assets/javascripts 以下を編集する
* spec/javascripts 以下にテストを書く

# 更新
※git pull というコマンドがあるけどこれは使わない。

更新作業は以下のように行います。

0. Bitbucket.org に繋いでSync
1. `git fetch --all`
1. `git checkout master`
1. `git merge origin/master`
1. `git checkout _YOUR-WORKING-BRANCH_`
1. `git rebase master`
2. edit
3. `git commit -am 'message'`
4. `git push orign _YOUR-WORKING-BRANCH_`
5. Pull Request

- bundle exec guard を使って効率的にテストする
- live-reload extension を使って効率的にデザインを実装する

# ルーティング
基本的には public/index.html ファイルが参照される

以下のような Router があった場合、コメントに示すようなURLにアクセスする。（_#に注意_）

```coffeescript
#*routs.js.coffee
class EatEatEat.Routers.Events extends Backbone.Router

  routes:
    '' : 'index'   # http://localhost:3000/ 
    'new': 'new'   # http://localhost:3000/#new
    ':id' : 'show' # http://localhost:3000/#12
```




# 参考
* HTML5 & CSS
    * https://developer.mozilla.org/ja/
    * http://www.html5.jp
* Backbone
    * http://backbonejs.org
    * http://railscasts.com/episodes/323-backbone-on-rails-part-1
    * http://railscasts.com/episodes/325-backbone-on-rails-part-2
* Underscore
    * http://underscorejs.org
* coffeescript
    * http://minghai.github.com/library/coffeescript/index.html
* jQuery
    * http://jquery.com
* Jasmine
    * http://pivotal.github.io/jasmine/
* Bootstrap
    * http://twitter.github.io/bootstrap/

# さいご
足りない情報は随時書き足す。
