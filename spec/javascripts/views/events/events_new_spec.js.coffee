#= require "views/events/events_new"
describe "EventsNew", ->
  beforeEach ->
    setFixtures '<div id="container"></div>'
    @model = new Backbone.Model
    @view = new EatEatEat.Views.EventsNew mode: @model

  describe 'Rendering', ->
    it 'returns the view object', ->
      expect(@view.render()).toEqual(@view)

    it 'produces the corrent HTML', ->
      @view.render()
      console.log @view
