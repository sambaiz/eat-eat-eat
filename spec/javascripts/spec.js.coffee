# This pulls in all your specs from the javascripts directory into Jasmine:
# 
# spec/javascripts/*_spec.js.coffee
# spec/javascripts/*_spec.js
# spec/javascripts/*_spec.js.erb
# IT IS UNLIKELY THAT YOU WILL NEED TO CHANGE THIS FILE
#
#=require jquery
#=require jquery_ujs
#=require twitter/bootstrap
#=require underscore
#=require backbone
#=require eat_eat_eat
#=require_tree ../../app/assets/templates
#=require_tree ../../app/assets/javascripts
#=require_tree ../../app/assets/javascripts/models
#=require_tree ../../app/assets/javascripts/collections
#=require_tree ../../app/assets/javascripts/views
#=require_tree ../../app/assets/javascripts/routers
#=require_tree ./
