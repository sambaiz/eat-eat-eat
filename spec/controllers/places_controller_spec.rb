require 'spec_helper'

describe PlacesController do

  # This should return the minimal set of attributes required to create a valid
  # Place. As you add validations to Place, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "name" => "MyString", "latlong" => '39.596,139.889' }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # PlacesController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  before do
    request.accept = 'application/json'
  end

  describe "GET" do
     it "response json" do
	get :index
	header = response.header
	expect(header["Content-Type"]).to match /application\/json/
     end
  end

  describe "GET index" do
    it "assigns all places as @places" do
      place = Place.create! valid_attributes
      a = get :index, {}, valid_session
      json = response.body
      expect(json).to eq [place].to_json
    end
  end

  describe "GET show" do
    it "assigns the requested place as @place" do
      place = Place.create! valid_attributes
      get :show, {:id => place.to_param}, valid_session
      json = response.body
      expect(json).to eq place.to_json
    end
  end


  describe "POST create" do
    describe "with valid params" do
      it "creates a new Place" do
        expect {
          post :create, {:place => valid_attributes}, valid_session
        }.to change(Place, :count).by(1)
      end
    end

    describe "with invalid params" do
      it "can't create" do
        # Trigger the behavior that occurs when invalid params are submitted
        # Place.any_instance.stub(:save).and_return(false)
        Place.any_instance.should_receive(:errors).at_least(1).and_return({"name" => "is invalid"})
        Place.any_instance.should_receive(:valid?).at_least(1).and_return(false)
        post :create, {:place => { "name" => "invalid value" }}, valid_session
	expect(response.status).to eq 422
	errors = JSON.parse(response.body)['errors']
	expect(errors).to have_key("name")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested place" do
        place = Place.create! valid_attributes
        # Assuming there are no other places in the database, this
        # specifies that the Place created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        Place.any_instance.should_receive(:update_attributes).with({ "name" => "MyString" })
        put :update, {:id => place.to_param, :place => { "name" => "MyString" }}, valid_session
	expect(response.status).to eq 204
      end
    end

    describe "with invalid params" do
      it "can't update" do
        place = Place.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        #Place.any_instance.stub(:save).and_return(false)
        Place.any_instance.should_receive(:errors).at_least(1).and_return({"name" => "is invalid"})
        Place.any_instance.should_receive(:valid?).at_least(1).and_return(false)
        put :update, {:id => place.to_param, :place => { "name" => "invalid value" }}, valid_session
	expect(response.status).to eq 422
	errors = JSON.parse(response.body)['errors']
	expect(errors).to have_key("name")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested place" do
      place = Place.create! valid_attributes
      expect {
        delete :destroy, {:id => place.to_param}, valid_session
      }.to change(Place, :count).by(-1)
    end
  end
end
