# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event do
    owner ""
    place ""
    title "MyString"
    created_at "2013-04-16 15:45:58"
    schedule_for "2013-04-16 15:45:58"
  end
end
