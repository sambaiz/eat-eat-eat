# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :entry, :class => 'Entry' do
    user nil
    event nil
  end
end
