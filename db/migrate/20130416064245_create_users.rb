class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :icon_url
      t.string :email
      t.string :token
      t.string :profile_url

      t.timestamps
    end
  end
end
