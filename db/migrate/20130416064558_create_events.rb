class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.references :user
      t.references :place
      t.string :title
      t.datetime :schedule_for

      t.timestamps
    end
  end
end
